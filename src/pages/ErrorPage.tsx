import * as React from 'react'

class ErrorPage extends React.Component {
    public render() {
        return <div className='404'>
            <h1>Запрашиваемая страница не существует</h1>
        </div>
    }
}

export default ErrorPage
