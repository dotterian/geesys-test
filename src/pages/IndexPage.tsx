import * as React from 'react';
import connect from 'react-redux-fetch';
import { Redirect } from 'react-router-dom';
import CompoundTag from 'src/components/compoundTag';
import { IState } from 'src/store';

interface IPropTypes {
    dispatchFormConfigGet: () => Promise<{}>,
    dispatchFormConfigPost: (formData: object) => Promise<{}>,
    formConfigFetch: IState['repository']['formConfig'],
}

class IndexPage extends React.Component<IPropTypes> {

    public componentDidMount() {
        this.props.dispatchFormConfigGet()
    }

    public render() {
        const { rejected, fulfilled, value, reason, pending } = this.props.formConfigFetch

        if (rejected) {
            if (reason && reason.cause.authentication) {
                return <>
                    <h1>Авторизация</h1>
                    <CompoundTag
                        config={reason.cause.authentication}
                        pending={pending}
                        onSubmit={this.onSubmit}
                    />
                </>
            }
            return <div className='error'>
                <h3>Произошла ошибка</h3>
                <button onClick={this.props.dispatchFormConfigGet}>Повторить запрос</button>
            </div>
        }

        if (fulfilled || value) {
            if (value) {
                if (value.authentication) {
                    return <>
                        <h1>Авторизация</h1>
                        <CompoundTag
                            config={value.authentication}
                            pending={pending}
                            onSubmit={this.onSubmit}
                        />
                    </>
                }
                if (value.accessToken) {
                    return <Redirect to='/user' />
                }
            }
            return <div className='error'>
                <h3>Сервер вернул неожиданные данные</h3>
                <button onClick={this.props.dispatchFormConfigGet}>Повторить запрос</button>
            </div>
        }

        return <div className='loader' />
    }

    protected onSubmit = (name: string, event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        const formData = new FormData(event.currentTarget)
        const jsonData = {}
        formData.forEach((value, key) => {
            const path = key.split(/[\[\]]/)
            let parent = jsonData
            let i = 1
            for (const part of path) {
                i++
                console.log(part)
                if (part !== '') {
                    if (!parent.hasOwnProperty(part)) {
                        if (path.length > i) {
                            parent[part] = {}
                        } else {
                            parent[part] = value
                        }
                    }
                    parent = parent[part]
                }
            }
        })
        this.props.dispatchFormConfigPost(jsonData)
    }
}

export default connect([{
    request: {
        url: 'https://api.vs12.nwaj.ru/v1/forms/post/user/authentication'
    },
    resource: 'formConfig'
}, {
    method: 'post',
    request: (formData) => {
        return {
            body: formData,
            url: 'https://api.vs12.nwaj.ru/v1/user/authentication'
        }
    },
    resource: 'formConfig'
}])(IndexPage)
