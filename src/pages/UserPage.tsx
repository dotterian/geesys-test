import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { IState } from '../store';

interface IPropTypes {
    accessToken: string | null
}

class UserPage extends React.Component<IPropTypes> {
    public render() {
        const { accessToken } = this.props
        console.log(this.props)
        if (!accessToken) {
            return <Redirect to='/' />
        }
        return <h1>Профиль пользователя</h1>
    }
}

export default connect((state: IState) => {
    let accessToken = null
    if (state.repository.formConfig &&
        state.repository.formConfig.value &&
        state.repository.formConfig.value.accessToken
    ) {
        accessToken = state.repository.formConfig.value.accessToken
    }
    return { accessToken }
})(UserPage)
