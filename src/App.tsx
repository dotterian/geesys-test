import * as React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import ErrorPage from './pages/ErrorPage';
import IndexPage from './pages/IndexPage';
import UserPage from './pages/UserPage';
import store from './store';

class App extends React.Component {
  public render() {
    return <Provider store={store}>
      <Router>
        <>
          <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
            <a className="navbar-brand" href="#">Geesys Test</a>
          </nav>
          <main className='container'>
            <Switch>
              <Route exact={true} path='/' component={IndexPage} />
              <Route exact={true} path='/user' component={UserPage} />
              <Route path='*' component={ErrorPage} />
            </Switch>
          </main>
        </>
      </Router>
    </Provider>
  }
}

export default App;
