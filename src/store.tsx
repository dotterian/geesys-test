import { middleware as fetchMiddleware, reducer as fetchReducer } from 'react-redux-fetch';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ICompoundTagConfig } from 'src/components/compoundTag';

export default createStore(
    combineReducers({
        repository: fetchReducer
    }),
    composeWithDevTools(
        applyMiddleware(fetchMiddleware)
    )
)

export interface IFetch<T> {
    rejected: boolean,
    fulfilled: boolean,
    pending: boolean,
    reason?: {
        cause: T
    }
    value?: T
}

export interface IState {
    repository: {
        formConfig: IFetch<{
            authentication?: ICompoundTagConfig
            accessToken?: string
        }>
    }
}
