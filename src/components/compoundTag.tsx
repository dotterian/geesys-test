import * as React from 'react'

export interface ICompoundTagConfig {
    input_type: string,
    id: string,
    full_name: string,
    disabled: boolean,
    label: string | null,
    attr: string[],
    valid: boolean,
    reqired: boolean,
    size: number | null,
    compound: boolean,
    value: string | null,
    submitted: boolean,
    errors: string[],
    short_name: string,
    children: ICompoundTagConfig[]
}

interface IPropTypes {
    config: ICompoundTagConfig,
    pending?: boolean,
    onSubmit?: (name: string, event: React.FormEvent<HTMLFormElement>) => void
}

class CompoundTag extends React.Component<IPropTypes> {
    public render() {
        const {
            valid, compound, submitted, children: childrenConfig,
            full_name, short_name, errors, label, value, input_type,
            ...config
        } = this.props.config
        const children: React.ReactNodeArray = []

        if (childrenConfig) {
            for (const tagConfig of childrenConfig) {
                children.push(<CompoundTag
                    key={tagConfig.full_name}
                    config={tagConfig}
                    onSubmit={this.props.onSubmit}
                />)
            }
        }

        if (compound) {
            if (errors.length > 0) {
                children.push(<div className='alert alert-danger' role='alert'>{errors}</div>)
            }
            children.push(<button key='submit' className='btn btn-primary' type='submit' disabled={this.props.pending}>Войти</button>)

            return React.createElement(
                'form',
                {
                    ...config,
                    name: full_name,
                    onSubmit: (e: React.FormEvent<HTMLFormElement>) => {
                        if (this.props.onSubmit) {
                            this.props.onSubmit(full_name, e)
                        }
                    }
                },
                children
            )
        }

        let className = 'form-control'

        if (errors.length > 0) {
            className += ' is-invalid'
        }

        return <div className='form-group'>
            <label>{label}</label>
            {React.createElement(
                'input',
                {
                    ...config,
                    className,
                    defaultValue: value,
                    key: full_name,
                    name: full_name,
                    type: input_type,
                }
            )}
            <span className='invalid-feedback'>{errors}</span>
        </div>
    }
}

export default CompoundTag
